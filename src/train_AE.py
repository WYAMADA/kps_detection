import cv2
import numpy as np
from dataset.dataloader import CSPSDataset
from model.AE import AE
from utils.improc import tensor_to_image
from torch.utils.data import DataLoader
import torch.optim as optim
from torch import nn
import torch

from tqdm import tqdm
from time import sleep

if __name__ == "__main__":

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    print("Device being used: ", device)
    
    print("Loading data...")
    dataset = CSPSDataset()
    train_data, test_data = torch.utils.data.random_split(dataset, [0.8, 0.2], generator=torch.Generator().manual_seed(42))
    print("Data loaded!")
    print("Total:\t", len(dataset))
    print("Train\t", len(train_data))
    print("Test:\t", len(test_data))

    train_loader = DataLoader(train_data, batch_size=32, shuffle=True)
    test_loader = DataLoader(test_data, batch_size=32, shuffle=True)

    print("Loading model...")
    model = AE(input_shape=(64,64,3)).to(device)
    print("Model loaded!")

    optimizer = optim.Adam(model.parameters(), lr=1e-3)

    criterion = nn.MSELoss()

    epochs = 100

    loss_list = []

    for epoch in range(epochs):
        with tqdm(train_loader, unit="batch") as tepoch:
            loss = 0
            for batch_features, _ in tepoch:
                batch_features = batch_features.to(device)
                # print("Batch Features: ", batch_features.shape)
                # print( batch_features)

                # reset the gradients back to zero
                # PyTorch accumulates gradients on subsequent backward passes
                optimizer.zero_grad()
                
                # compute reconstructions
                outputs = model(batch_features)
                
                # compute training reconstruction loss
                train_loss = criterion(outputs, batch_features)
                
                # compute accumulated gradients
                train_loss.backward()
                
                # perform parameter update based on current gradients
                optimizer.step()
                
                # add the mini-batch training loss to epoch loss
                loss += train_loss.item()
                    
            # compute the epoch training loss
            loss = loss / len(train_loader)
            loss_list.append(loss)

            # display the epoch training loss
            print("epoch : {}/{}, loss = {:.6f}".format(epoch + 1, epochs, loss))

            tepoch.set_postfix(loss=loss)
            sleep(0.1)

            if (epoch//5 == 0):
                torch.save(model.state_dict(), "output/AE.pt")
    
    with open("output/loss.npy", "wb") as f: 
        np.save(f,np.array(loss_list))
    torch.save(model.state_dict(), "output/AE.pt")