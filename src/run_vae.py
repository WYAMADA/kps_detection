import cv2
import torch
import lightning
import torch
from dataset.dataloader import CSPSDataset
from model.VAE import VAE
from torch.utils.data import DataLoader
from torchvision.utils import make_grid
import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm
import argparse
import csv

def tensor_to_image(tensor_input):
    out= make_grid(tensor_input).permute(1, 2, 0).numpy()*255
    return out.astype(np.uint8)

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser(
        prog = 'VAE runner',
        description = "Run VAE on test set",)
    parser.add_argument('--save_image', action='store_true')
    args = parser.parse_args()

    print("Loading model...")
    try:
        vae = VAE.load_from_checkpoint("src/model/vae.ckpt")
    except:
        raise("Invalid model")
    print("... model loaded successfully!")

    print("Loading Dataset...")
    dataset = CSPSDataset(resize=True,size=128)
    train_data, test_data = torch.utils.data.random_split(dataset, [0.8, 0.2], generator=torch.Generator().manual_seed(42))
    test_data = train_data
    print("Data loaded!")
    print("Total:\t", len(dataset))
    print("Train\t", len(train_data))
    print("Test:\t", len(test_data))

    test_loader = DataLoader(test_data, batch_size=1, shuffle=True)
    print("... dataset loaded successfully!")

    if(args.save_image):
        for i in tqdm(range(20)):
            file_name = str(i).zfill(4)+"_"+test_data[i][1]
            img = test_data[i][0]
            img = img[None,:]
            plt.imshow(tensor_to_image(img.cpu()))
            plt.savefig("output/VAE/"+file_name+"_in.jpeg")
            with torch.no_grad():
                x_encoded = vae.encoder(img)
                mu, log_var = vae.fc_mu(x_encoded), vae.fc_var(x_encoded)
                std = torch.exp(log_var / 2)
                q = torch.distributions.Normal(mu, std)
                z = q.rsample()
                x_hat = vae.decoder(z)
            output = x_hat.cpu()
            plt.imshow(tensor_to_image(output))
            plt.savefig("output/VAE/"+file_name+"_out.jpeg")
    else:
        label = []
        x_hat_list = []
        mu_list = []
        std_list = []
        
        for i in tqdm(range(len(test_data))):
            label.append(test_data[i][1])
            img = test_data[i][0]
            img = img[None,:]
            with torch.no_grad():
                x_encoded = vae.encoder(img)
                x_hat_list.append(x_encoded.squeeze().numpy())
                mu, log_var = vae.fc_mu(x_encoded), vae.fc_var(x_encoded)
                std = torch.exp(log_var / 2)
                mu_list.append(mu.squeeze().numpy())
                std_list.append(std.squeeze().numpy())
        
        x_hat_list = np.asarray(x_hat_list)
        mu_list = np.asarray(mu_list)
        std_list = np.asarray(std_list)

        with open('output/VAE/label.csv', 'w') as myfile:
            wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
            wr.writerow(label)

        np.savetxt("output/VAE/x_encoded.csv", x_hat_list, delimiter=",")
        np.savetxt("output/VAE/mu_list.csv", mu_list, delimiter=",")
        np.savetxt("output/VAE/std_list.csv", std_list, delimiter=",")