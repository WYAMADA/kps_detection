import cv2
import numpy as np
from dataset.dataloader import CSPSDataset
from model.AE import AE
from utils.improc import tensor_to_image
from torch.utils.data import DataLoader
import torch.optim as optim
from torch import nn
import torch
import matplotlib.pyplot as plt

from tqdm import tqdm
from time import sleep

def save_image(path,image):
    cv2.imwrite(path, image)

if __name__ == "__main__":

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    print("Device being used: ", device)
    
    print("Loading data...")
    dataset = CSPSDataset()
    train_data, test_data = torch.utils.data.random_split(dataset, [0.8, 0.2], generator=torch.Generator().manual_seed(42))
    print("Data loaded!")
    print("Total:\t", len(dataset))
    print("Train\t", len(train_data))
    print("Test:\t", len(test_data))

    train_loader = DataLoader(train_data, batch_size=1, shuffle=True)
    test_loader = DataLoader(test_data, batch_size=1, shuffle=True)

    model = AE(input_shape=(64,64,3)).to(device)
    model.load_state_dict(torch.load("output/AE.pt"))
    model.eval()

    for i in range(10):
        test_features, test_labels = next(iter(train_loader))

        # print("Test features shape: ", test_features.shape)
        # print("Test features label: ", test_labels)

        outputs = model(test_features.to(device))
        # print("Output features shape: ", outputs.shape)
        image_in = tensor_to_image(torch.squeeze(test_features).to("cpu"))
        image_out = tensor_to_image(torch.squeeze(outputs).to("cpu"))

        path_in = "output/AE/"+str(i)+"_"+test_labels[0]+"_input.jpeg"
        path_out = "output/AE/"+str(i)+"_"+test_labels[0]+"_output.jpeg"
        save_image(path_in, image_in)
        save_image(path_out, image_out)

    loss = np.load("output/loss.npy")
    
    plt.plot(loss)
    plt.grid()
    plt.xlabel("Epoch")
    plt.ylabel("Loss")
    plt.savefig("output/AE/loss.jpeg")

    # for epoch in range(epochs):
    #     with tqdm(test_loader, unit="batch") as tepoch:
    #         loss = 0
    #         for batch_features, _ in tepoch:
                
    #             batch_features = batch_features.to(device)
                
    #             outputs = model(batch_features)
                
    #             # compute training reconstruction loss
    #             train_loss = criterion(outputs, batch_features)
                
    #             # compute accumulated gradients
    #             train_loss.backward()
                
    #             # perform parameter update based on current gradients
    #             optimizer.step()
                
    #             # add the mini-batch training loss to epoch loss
    #             loss += train_loss.item()
                    
    #         # compute the epoch training loss
    #         loss = loss / len(train_loader)
    #         loss_list.append(loss)

    #         # display the epoch training loss
    #         print("epoch : {}/{}, loss = {:.6f}".format(epoch + 1, epochs, loss))

    #         tepoch.set_postfix(loss=loss)
    #         sleep(0.1)