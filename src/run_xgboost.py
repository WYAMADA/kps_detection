import numpy as np
import matplotlib.pyplot as plt
import xgboost as xgb
from xgboost import XGBClassifier
import csv
import argparse
import pandas as pd
import os

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('-p','--path', default="output/VAE/")
    parser.add_argument('-l','--label', default="label.csv")
    parser.add_argument('-m','--mu', default="mu_list.csv")
    parser.add_argument('-s','--std', default="std_list.csv")
    parser.add_argument('-x','--x_encoded', default="x_encoded.csv")
    
    args = parser.parse_args()

    label_path = os.path.join(args.path,args.label)
    mu_path = os.path.join(args.path,args.mu)
    std_path = os.path.join(args.path,args.std)
    x_path = os.path.join(args.path,args.x_encoded)

    label = pd.read_csv(label_path, sep=',',header=None).T
    label = label.astype("category")

    le = LabelEncoder()
    y = le.fit_transform(label)

    # print(len(np.unique(y)))

    x_encoded = pd.read_csv(x_path, sep=',',header=None)
    mu = pd.read_csv(mu_path, sep=',',header=None)
    std = pd.read_csv(std_path, sep=',',header=None)

    X_train, X_test, y_train, y_test = train_test_split(x_encoded, y, test_size=.2,seed=42)

    dtrain = xgb.DMatrix(X_train, label=y_train)
    dtest = xgb.DMatrix(X_test, label=y_test)

    evallist = [(dtrain, 'train'), (dtest, 'eval')]

    # clf = xgb.XGBClassifier(tree_method="gpu_hist", enable_categorical=True)
    # clf.fit(X_train, y_train)
    # clf.save_model("categorical-model.json")

    params = {
        'objective': 'multi:softprob',
        'tree_method': 'gpu_hist',
        'num_class': 46,
        'seed': 42,
        'max_depth': 2,
        # 'colsample_bytree': 0.36524046160303747,
        # 'colsample_bylevel': 0.7008644188368828,
        'grow_policy': 'lossguide',
        # 'lambda': 1e-8,
        'alpha': 0.1,
        'subsample': 0.9,
        'eta': 0.01,
        'eval_metric': ['merror','mlogloss']
    }

    bst = xgb.train(params, dtrain, 
            num_boost_round=100, 
            verbose_eval=10, 
            early_stopping_rounds=10, 
            evals=evallist
            )

    bst.save_model('output/XGB/0001.model')
    xgb.plot_importance(bst)
    plt.savefig('output/XGB/importance.jpeg')
    xgb.plot_tree(bst, num_trees=2)
    plt.savefig('output/XGB/tree.jpeg')
