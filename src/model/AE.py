import torch
from torch import nn

class AE(nn.Module):
    def __init__(self, **kwargs):
        super().__init__()

        feat_size = kwargs["input_shape"][0]*kwargs["input_shape"][1]*kwargs["input_shape"][2]
        print("Feature size:\t",feat_size)

        self.flat = nn.Flatten()
        self.encoder_hidden_layer = nn.Linear(
            in_features=feat_size, out_features=128
        )
        self.encoder_output_layer = nn.Linear(
            in_features=128, out_features=128
        )
        self.decoder_hidden_layer = nn.Linear(
            in_features=128, out_features=128
        )
        self.decoder_output_layer = nn.Linear(
            in_features=128, out_features=feat_size
        )
        self.unflat = nn.Unflatten(1,(kwargs["input_shape"][2],kwargs["input_shape"][0],kwargs["input_shape"][1]))

    def forward(self, features):
        # print("Features",features.shape)
        flattened = self.flat(features)
        # print("Flattened",flattened.shape)
        activation = self.encoder_hidden_layer(flattened)
        activation = torch.relu(activation)
        code = self.encoder_output_layer(activation)
        code = torch.relu(code)
        activation = self.decoder_hidden_layer(code)
        activation = torch.relu(activation)
        activation = self.decoder_output_layer(activation)
        reconstructed = torch.relu(activation)
        unflattened = self.unflat(reconstructed)
        return unflattened