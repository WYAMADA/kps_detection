#!/bin/bash

echo Running VAE Docker...
docker compose -f docker-compose-vae.yml down

if [ "$1" == "" ] || [ $# -gt 1]
then
    echo No build needed...
    docker compose -f docker-compose-xgb.yml up -d
elif [ "$1" == build ]
then
    echo Building and launching
    docker compose -f docker-compose-xgb.yml up -d --build
else
    echo Invalid input argument
fi