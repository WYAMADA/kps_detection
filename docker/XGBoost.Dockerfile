FROM pytorch/pytorch:1.13.1-cuda11.6-cudnn8-devel

RUN mkdir /workspace/CSPS
RUN apt-get update && apt-get install ffmpeg libsm6 libxext6  -y
RUN pip install opencv-python
RUN pip install torchsummary
RUN pip install tqdm
RUN pip install matplotlib

RUN apt install -y git

ENV PATH="/ust/local/cuda/bin:$PATH"

RUN pip install seaborn
RUN pip install pandas
RUN pip install xgboost
RUN pip install scikit-learn scipy
RUN pip install graphviz
RUN apt install -y graphviz