FROM pytorch/pytorch:1.13.1-cuda11.6-cudnn8-devel

RUN mkdir /workspace/CSPS
# RUN mkdir /workspace/CSPS/data
# RUN mkdir /workspace/CSPS/src
RUN apt-get update && apt-get install ffmpeg libsm6 libxext6  -y
RUN pip install opencv-python
RUN pip install torchsummary
RUN pip install tqdm
RUN pip install matplotlib

RUN apt install -y git

ENV PATH="/ust/local/cuda/bin:$PATH"

RUN python -m pip install lightning
RUN python -m pip install lightning-bolts

RUN rm /opt/conda/lib/python3.10/site-packages/pl_bolts/callbacks/data_monitor.py
COPY ./src/data_monitor.py /opt/conda/lib/python3.10/site-packages/pl_bolts/callbacks/data_monitor.py